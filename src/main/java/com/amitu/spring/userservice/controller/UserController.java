package com.amitu.spring.userservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.amitu.spring.userservice.model.User;
import com.amitu.spring.userservice.repository.UserRepository;

@RestController
public class UserController {
	
	@Autowired 
	private UserRepository repo;

	@RequestMapping(path = "greetings", method = RequestMethod.GET)
	public String greetings() {
		return "Hello World";
	}
	
	@PostMapping("/users")
	public ResponseEntity<User> addUser(@RequestBody User user) {
		
		repo.save(user);
		
		return new ResponseEntity<>(user,HttpStatus.CREATED );
	}
	
}
